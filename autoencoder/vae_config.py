import torch
import torchvision.transforms as transforms

DEVICE = "cuda" if torch.cuda.is_available() else "cpu"

IN_CHANNELS = 3
IMAGE_SIZE = 128
EMBEDDING_SIZE = 256
DATASET_PATH = "./data/frames"
TEST_PATH = "./data/test_frames"
CHECKPOINT_DIR = "./trains"
CHECKPOINT_NAME = "autoencoder.pth.tar"

NUM_EPOCHS = 100
LEARNING_RATE = 3e-4
BATCH_SIZE = 32
NUM_WORKERS = 2

LOAD_MODEL = False
SAVE_MODEL = True

MEAN = [0.5, 0.5, 0.5]
STD = [0.5, 0.5, 0.5]
# REAL_MEAN = [0.5040, 0.5266, 0.4579]
# REAL_STD = [0.2944, 0.2927, 0.3290]

train_transform = transforms.Compose([
    transforms.Resize((IMAGE_SIZE, IMAGE_SIZE)),
    transforms.ToTensor(),
    transforms.Normalize(mean=MEAN, std=STD),
])

test_transform = transforms.Compose([
    transforms.Resize((IMAGE_SIZE, IMAGE_SIZE)),
    transforms.ToTensor(),
    transforms.Normalize(mean=MEAN, std=STD),
])
