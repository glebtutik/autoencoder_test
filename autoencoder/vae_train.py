import vae_config
from tqdm import tqdm
from vae_utils import *
from vae_model import Autoencoder
from torch import optim, nn, Tensor
from torch.utils.data import DataLoader
from torchvision.utils import make_grid
from vae_dataset import CartoonFramesDataset
from torch.utils.tensorboard import SummaryWriter


def loss_function(x_hat, x, mu, logvar) -> Tensor:
    MSE = nn.functional.mse_loss(x_hat, x, reduction='sum')
    KLD = -0.5 * torch.sum(1 + logvar - mu.pow(2) - logvar.exp())
    return MSE + KLD


def test(model, data_loader, device="cpu"):
    model.eval()

    input_images = []
    output_images = []
    with torch.no_grad():
        for idx, input_batch in enumerate(data_loader):
            input_batch = input_batch.to(device)
            output_batch, _, _ = model(input_batch)
            input_images.append(input_batch)
            output_images.append(output_batch)

    model.train()

    input_images = [torch.tensor(postprocessing(img)) for img in input_images]
    output_images = [torch.tensor(postprocessing(img)) for img in output_images]

    input_images = torch.cat(input_images, dim=0)
    output_images = torch.cat(output_images, dim=0)
    input_grid = make_grid(input_images, nrow=4, padding=5, pad_value=1)
    output_grid = make_grid(output_images, nrow=4, padding=5, pad_value=1)
    combined_grid = np.concatenate([input_grid.numpy(), output_grid.numpy()], axis=2)

    return combined_grid


def train(model, opt, train_loader, test_loader, num_epochs, device="cpu", criterion=None, writer=None):
    if not criterion:
        criterion = loss_function

    for epoch in range(num_epochs):

        current_loss = 0.0

        loop = tqdm(train_loader)
        for idx, input_images in enumerate(loop):
            input_images = input_images.to(device)

            # Вычисляем mu, logvar и z:
            output_images, mu, logvar = model(input_images)

            # Вычисляем ELBO и потери:
            loss = criterion(output_images, input_images, mu, logvar)

            opt.zero_grad()
            loss.backward()
            opt.step()

            current_loss += loss.item()

            # Обновляем tensorboard (текущие изображения)
            if writer is not None and idx % 64 == 0:
                global_step = (epoch * len(train_loader) + idx) * len(input_images)

                if idx != 0:
                    writer.add_scalar("Loss", current_loss, global_step=global_step)
                    current_loss = 0

                test_images = test(model, test_loader, device)
                # test_images = np.concatenate(test_images, axis=1)
                writer.add_image("Test images", test_images, global_step=global_step)


def main():
    set_seed()
    device = vae_config.DEVICE

    # Инициализируем модель:
    model = Autoencoder(embedding_size=vae_config.EMBEDDING_SIZE).to(device)

    # Инициализируем оптимизатор:
    opt = optim.Adam(params=model.parameters(), lr=vae_config.LEARNING_RATE)

    # Загружаем датасет:
    train_dataset = CartoonFramesDataset(root_dir=vae_config.DATASET_PATH, transform=vae_config.train_transform)
    test_dataset = CartoonFramesDataset(root_dir=vae_config.TEST_PATH, transform=vae_config.test_transform)

    train_loader = DataLoader(
        dataset=train_dataset,
        batch_size=vae_config.BATCH_SIZE,
        shuffle=True,
        num_workers=vae_config.NUM_WORKERS,
        pin_memory=True
    )
    test_loader = DataLoader(
        dataset=test_dataset,
        batch_size=1,
        num_workers=vae_config.NUM_WORKERS
    )

    num_epochs = vae_config.NUM_EPOCHS  # Количество эпох обучения

    writer = SummaryWriter(f"./tb/{get_current_time()}")

    # Загружаем последний чекпоинт модели:
    if vae_config.LOAD_MODEL:
        print("\033[32m=> Загрузка последнего чекпоинта\033[0m")

        checkpoint_path = get_last_checkpoint()
        model, opt = load_checkpoint(model, opt, checkpoint_path)

    # Обучаем модель:
    try:
        train(model, opt, train_loader, test_loader, num_epochs, device=device, writer=writer)
    except KeyboardInterrupt:
        if vae_config.SAVE_MODEL:
            print("\033[32m=> Сохранение чекпоинта\033[0m")

            # Создаем директорию для сохранения
            dir_name = get_current_time()
            dir_path = os.path.join(vae_config.CHECKPOINT_DIR, dir_name)
            make_directory(dir_path)

            # Сохраняем
            model_path = os.path.join(dir_path, vae_config.CHECKPOINT_NAME)
            save_checkpoint(model, opt, model_path)

        writer.close()


if __name__ == "__main__":
    main()
