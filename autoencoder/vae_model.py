import torch
import torch.nn as nn
from torch import Tensor
from typing import Tuple

from torchsummary import summary


class ConvBlock(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride, padding, activation="relu", transpose=False):
        super(ConvBlock, self).__init__()

        conv_layer = nn.ConvTranspose2d if transpose else nn.Conv2d
        conv_kwargs = {'output_padding': (stride - 1)} if transpose else {}

        self.block = nn.Sequential(
            conv_layer(in_channels, out_channels, kernel_size, stride, padding, **conv_kwargs),
            nn.BatchNorm2d(out_channels),
            nn.ReLU() if activation == "relu" else nn.Tanh()
        )

    def forward(self, x: Tensor) -> Tensor:
        return self.block(x)


class Encoder(nn.Module):
    def __init__(self, in_channels: int, embedding_size: int):
        super(Encoder, self).__init__()

        self.net = nn.Sequential(
            ConvBlock(in_channels, 32, kernel_size=3, stride=2, padding=1),
            ConvBlock(32, 64, kernel_size=3, stride=2, padding=1),
            ConvBlock(64, 128, kernel_size=3, stride=2, padding=1),
            ConvBlock(128, embedding_size, kernel_size=3, stride=2, padding=1),
        )

    def forward(self, x: Tensor) -> Tensor:
        return self.net(x)


class Decoder(nn.Module):
    def __init__(self, embedding_size: int):
        super(Decoder, self).__init__()

        self.net = nn.Sequential(
            ConvBlock(embedding_size, 128, kernel_size=3, stride=2, padding=1, transpose=True),
            ConvBlock(128, 64, kernel_size=3, stride=2, padding=1, transpose=True),
            ConvBlock(64, 32, kernel_size=3, stride=2, padding=1, transpose=True),
            ConvBlock(32, 3, kernel_size=3, stride=2, padding=1, activation="tanh", transpose=True)
        )

    def forward(self, z: Tensor) -> Tensor:
        return self.net(z)


class Autoencoder(nn.Module):
    def __init__(self, in_channels: int = 3, embedding_size: int = 1024):
        super(Autoencoder, self).__init__()

        self.encoder = Encoder(in_channels, embedding_size)
        self.hidden = nn.Conv2d(embedding_size, embedding_size, kernel_size=1)
        self.logvar = nn.Conv2d(embedding_size, embedding_size, kernel_size=1)
        self.decoder = Decoder(embedding_size)

    def reparameterize(self, mu: Tensor, logvar: Tensor) -> Tensor:
        std = torch.exp(0.5 * logvar)
        eps = torch.randn_like(std)
        return eps * std + mu

    def forward(self, x: Tensor) -> Tuple[Tensor, Tensor, Tensor]:
        z = self.encoder(x)
        mu, logvar = self.hidden(z), self.logvar(z)
        z = self.reparameterize(mu, logvar)
        x_hat = self.decoder(z)
        return x_hat, mu, logvar


if __name__ == "__main__":
    model = Autoencoder()

    summary(model, (3, 128, 128))
