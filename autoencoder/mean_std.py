import torch
from tqdm import tqdm
from torch.utils.data import DataLoader
import vae_config
from torchvision.transforms import transforms
from vae_dataset import CartoonFramesDataset


def calculate_mean_std(dataloader):
    channels_sum, channels_squared_sum, num_batches = 0, 0, 0

    for data in tqdm(dataloader):
        images = data
        channels_sum += torch.mean(images, dim=[0, 2, 3])
        channels_squared_sum += torch.mean(images ** 2, dim=[0, 2, 3])
        num_batches += 1

    mean = channels_sum / num_batches
    std = (channels_squared_sum / num_batches - mean ** 2) ** 0.5

    return mean, std


def main():
    dataset = CartoonFramesDataset(
        root_dir=autoencoder_config.DATASET_PATH,
        transform=transforms.ToTensor()
    )

    data_loader = DataLoader(
        dataset=dataset,
        batch_size=autoencoder_config.BATCH_SIZE,
        num_workers=autoencoder_config.NUM_WORKERS,
    )

    mean, std = calculate_mean_std(data_loader)

    print(f"Mean is {mean}")
    print(f"Std is {std}")


if __name__ == "__main__":
    main()
