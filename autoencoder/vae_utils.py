import datetime
import os
import sys
import torch
import shutil
import random
import numpy as np
import vae_config
from datetime import datetime
import torch.nn.functional as F


def make_directory(dir_path: str) -> None:
    """Создаёт директорию. Если директория существует - перезаписывает."""

    try:
        os.makedirs(dir_path)
    except FileExistsError:
        shutil.rmtree(dir_path)
        os.makedirs(dir_path)


def get_current_time() -> str:
    return datetime.now().strftime("%Y-%m-%d_%H-%M-%S")


def save_checkpoint(model, optimizer, model_path) -> None:
    """Сохраняет чекпоинт модели в процессе обучения (модель, оптимизатор, номер эпохи)."""

    checkpoint = {
        "model_state_dict": model.state_dict(),
        "optimizer_state_dict": optimizer.state_dict()
    }
    torch.save(checkpoint, model_path)


def load_checkpoint(model, optimizer, checkpoint_file):
    """Загружает чекпоинт модели. Возвращает модель, оптимизатор, номер эпохи"""

    if not os.path.isfile(checkpoint_file):
        raise FileNotFoundError(f"Ошибка: не удалось найти {checkpoint_file}")

    checkpoint = torch.load(checkpoint_file, map_location=vae_config.DEVICE)

    model.load_state_dict(checkpoint["model_state_dict"])
    optimizer.load_state_dict(checkpoint["optimizer_state_dict"])

    return model, optimizer


def get_last_checkpoint() -> str:
    """Возвращает путь к последнему по времени сохранённому чекпоинту."""

    try:
        checkpoints = [d for d in os.listdir(vae_config.CHECKPOINT_DIR) if os.path.isdir(os.path.join(vae_config.CHECKPOINT_DIR, d))]
        if not checkpoints:
            raise IndexError
        checkpoints.sort(key=lambda l: os.path.getmtime(os.path.join(vae_config.CHECKPOINT_DIR, l)))  # Сортировка по времени
        path_to_model = os.path.join(vae_config.CHECKPOINT_DIR, checkpoints[-1], vae_config.CHECKPOINT_NAME)
        return path_to_model
    except IndexError:
        print(f"Ошибка: в директории {vae_config.CHECKPOINT_DIR} нет сохраненных чекпоинтов")
        sys.exit(1)
    except FileNotFoundError:
        print(f'Ошибка: не удалось загрузить {vae_config.CHECKPOINT_NAME}')
        sys.exit(1)


def postprocessing(tensors, image_size=(80, 140)):
    if len(tensors.shape) == 3:
        tensors = tensors[None, :, :, :]

    tensors = torch.nn.functional.interpolate(tensors, size=image_size, mode='bilinear', align_corners=False)
    tensors = tensors.cpu().detach().numpy()

    for t in tensors:
        for channel in range(vae_config.IN_CHANNELS):
            t[channel] = t[channel] * vae_config.STD[channel] + vae_config.MEAN[channel]

    tensors = np.clip(tensors, 0, 1)
    tensors = (tensors * 255).astype(np.uint8)

    return tensors


def set_seed(seed: int = 42) -> None:
    np.random.seed(seed)
    random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False
    os.environ["PYTHONHASHSEED"] = str(seed)

    print(f"Random seed set as {seed}")
